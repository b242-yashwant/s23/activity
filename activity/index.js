console.log("Jay shree ram");
/*1. In the S23 folder, create an activity folder and an index.html and script.js file inside of it.

2. Link the script.js file to the index.html file.

3. Create a trainer object using object literals.

4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)*/
let trainer={
	Name:"Ash Ketchum",
	Age:10,
	Pokemon:["Pikachu","Charizard","Squirtle","Bulbasaur"],
	Friends:{
		hoenn:["May","Max"],
		kanto:["Brock","Mistry"]
	},
	talk:function(){
		console.log(this.Pokemon[0]+"! I choose you!")
	}
}
console.log(trainer);
trainer.talk();

/*5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!

6. Access the trainer object properties using dot and square bracket notation.*/

console.log("Result of dot notation:");
console.log(trainer.Name);
console.log("Result of square bracket notation:");
console.log(trainer['Pokemon']);
//. Invoke/call the trainer talk object method.
trainer.talk();

/*8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)*/

function Pokemon(name, level){
	this.name=name;
	this.level=level;
	this.Health=2*level;
    this.Attack= level;
    
    
    
    this.tackle=function(target){
    	console.log(this.name+' '+"tackled"+' '+target.name)
    	 target.Health=(target.Health-this.Attack);
    	console.log(target.name+`'s health is now reduced to ${target.Health}`);

    	 target.faint=function(target){
    	console.log(target.name+' '+"has fainted.")};

    	if(target.Health<=0){
             target.faint(target);
    	}
    	console.log(target);
    };
   

    
}

let pikachu= new Pokemon('Pikachu',12);
console.log(pikachu);
let geodude=new Pokemon('Geodude',8);
console.log(geodude);

let mewtwo=new Pokemon('Mewtwo',100);
console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);

/*9. Create/instantiate several pokemon object from the constructor with varying name and level properties.*/

/*10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.*/

/*11. Create a faint method that will print out a message of targetPokemon has fainted.*/

/*12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.*/

//13. Invoke the tackle method of one pokemon object to see if it works as intended.
